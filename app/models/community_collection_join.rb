class CommunityCollectionJoin < DspaceRecord
  self.table_name = 'community2collection'
  self.primary_key = 'id'
  self.sequence_name = 'community2collection_seq'

  belongs_to :collection
  belongs_to :community

end