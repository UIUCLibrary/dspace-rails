class MetadataField < DspaceRecord
  self.table_name = 'metadatafieldregistry'
  self.primary_key = 'metadata_field_id'
  self.sequence_name = 'metadatafieldregistry_seq'

  belongs_to :metadata_schema
  has_many :metadata_values
end