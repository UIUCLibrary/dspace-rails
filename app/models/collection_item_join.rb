class CollectionItemJoin < DspaceRecord
  self.table_name = 'collection2item'
  self.sequence_name = 'collection2item_seq'
  self.primary_key = 'id'

  belongs_to :item, class_name: 'Item', foreign_key: 'item_id'
  belongs_to :collection, class_name: 'Collection', foreign_key: 'collection_id'

end