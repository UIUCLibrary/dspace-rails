class Item < DspaceRecord
  include Handleable
  self.table_name = 'item'
  self.primary_key = 'item_id'
  self.sequence_name = 'item_seq'

  belongs_to :submitter, class_name: 'EPerson', foreign_key: 'submitter_id'
  #Can't just call this owning_collection because that is a db field
  belongs_to :my_owning_collection, class_name: 'Collection', foreign_key: 'owning_collection'
  has_many :collection_item_joins, class_name: 'CollectionItemJoin', foreign_key: 'item_id'
  has_many :collections, through: :collection_item_joins
  has_many :item_bundle_joins
  has_many :bundles, through: :item_bundle_joins
  has_many :metadata_values
  has_many :workflows

  def add_item_to_collection(collection_id)
    self.collections<< Collection.find(collection_id)
  end

  def getBundleIds
    id_array=Array.new
    self.bundles.each do |i|
      id_array<<i.bundle_id
    end
    return id_array
  end

  def makePrivate
    self.discoverable=false
  end

end