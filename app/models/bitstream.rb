class Bitstream < DspaceRecord
  self.table_name = 'bitstream'
  self.sequence_name = 'bitstream_seq'
  self.primary_key = 'bitstream_id'

  has_one :bundle_bitstream_join
  has_one :bundle, through: :bundle_bitstream_join
  belongs_to :bitstream_format
  has_many :checksum_histories
  has_one :most_recent_checksum

end