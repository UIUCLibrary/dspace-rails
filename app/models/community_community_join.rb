class CommunityCommunityJoin < DspaceRecord
  self.table_name = 'community2community'
  self.primary_key = 'id'
  self.sequence_name = 'community2community_seq'

  belongs_to :parent_community, class_name: 'Community', foreign_key: 'parent_comm_id'
  belongs_to :child_community, class_name: 'Community', foreign_key: 'child_comm_id'

end