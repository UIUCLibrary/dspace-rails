class FileExtension < DspaceRecord
  self.table_name = 'fileextension'
  self.primary_key = 'file_extension_id'
  self.sequence_name = 'fileextension_seq'

  belongs_to :bitstream_format

end