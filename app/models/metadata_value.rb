class MetadataValue < DspaceRecord
  self.table_name = 'metadatavalue'
  self.primary_key = 'metadata_value_id'
  self.sequence_name = 'metadatavalue_seq'

  belongs_to :item
  belongs_to :metadata_field
end