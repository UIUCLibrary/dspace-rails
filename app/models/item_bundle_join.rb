class ItemBundleJoin < DspaceRecord
  self.table_name = 'item2bundle'
  self.primary_key = 'id'
  self.sequence_name = 'item2bundle_seq'

  belongs_to :item
  belongs_to :bundle
end