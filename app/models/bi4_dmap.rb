class Bi4Dmap < DspaceRecord
  self.table_name = 'bi_4_dmap'
  self.primary_key = 'map_id'
  self.sequence_name = 'bi_4_dmap_seq'

  belongs_to :item
  belongs_to :bi4_dis, foreign_key: 'distinct_id'

end