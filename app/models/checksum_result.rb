class ChecksumResult < DspaceRecord
  self.table_name = 'checksum_results'
  self.primary_key = 'result_code'

end