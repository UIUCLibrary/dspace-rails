#This class is over a view, so you shouldn't try to modify or create. Outbound associations are
#provided, but not inbound.
#If it does what the name suggests then you can pretty easily do this with the other classes,
#but it may be more efficient and/or convenient to use this at times. But I bet anything this
#gives you you can do as well with minimal loss on those fronts through the standard mechanisms.
class CommunityItemJoin < ActiveRecord::Base
  self.table_name = 'community2item'

  belongs_to :community
  belongs_to :item
end