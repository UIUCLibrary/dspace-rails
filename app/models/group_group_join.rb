#this actually joins EPersonGroups, but this naming is consistent with the database
class GroupGroupJoin < DspaceRecord
  self.table_name = 'group2group'
  self.primary_key = 'id'
  self.sequence_name = 'group2group_seq'

  belongs_to :parent_group, class_name: 'EPersonGroup', foreign_key: 'parent_id'
  belongs_to :child_group, class_name: 'EPersonGroup', foreign_key: 'child_id'
end