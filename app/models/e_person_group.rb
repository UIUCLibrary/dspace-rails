class EPersonGroup < DspaceRecord
  self.table_name = 'epersongroup'
  self.sequence_name = 'epersongroup_seq'
  self.primary_key = 'eperson_group_id'

  has_many :e_person_e_person_group_joins, class_name: 'EPersonEPersonGroupJoin', foreign_key: 'eperson_group_id'
  has_many :e_persons, :through => :e_person_e_person_group_joins
  has_many :e_person_group_workspace_item_joins, :foreign_key => 'eperson_group_id'
  has_many :workspace_items, through: :e_person_group_workspace_item_joins
  has_many :child_group_joins, class_name: 'GroupGroupJoin', foreign_key: 'parent_id'
  has_many :child_groups, through: :child_group_joins
  has_many :parent_group_joins, class_name: 'GroupGroupJoin', foreign_key: 'child_id'
  has_many :parent_groups, through: :parent_group_joins
  has_many :cached_child_group_joins, class_name: 'GroupGroupCacheJoin', foreign_key: 'parent_id'
  has_many :cached_child_groups, through: :child_group_joins
  has_many :cached_parent_group_joins, class_name: 'GroupGroupCacheJoin', foreign_key: 'child_id'
  has_many :cached_parent_groups, through: :parent_group_joins
  has_many :resource_policies, foreign_key: 'epersongroup_id'

end