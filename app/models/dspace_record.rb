#Because DSpace doesn't use the PRIMARY KEY facility of postgres we need
#to get and assign the primary key id before creating an object.
#The adapter doesn't seem to do this automatically (not totally surprising, but I'd still
# wish it would for the use case here where we're wrapping an existing db).
#Assuming we've set the primary key and sequence_name fields this is no problem.
class DspaceRecord < ActiveRecord::Base
  self.abstract_class = true
  before_create :assign_id

  def assign_id
    self.send("#{self.class.primary_key}=", self.next_id) unless self.send((self.class.primary_key))
  end

  def next_id
    self.connection.select("SELECT nextval('#{self.class.sequence_name}');").first['nextval']
  end

  def to_table

  end
end