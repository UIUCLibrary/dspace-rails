class BiItem < DspaceRecord
  self.table_name = 'bi_item'
  self.primary_key = 'id'
  self.sequence_name = 'bi_item_seq'

  belongs_to :item
end