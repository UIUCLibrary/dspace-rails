class Collection < DspaceRecord
  include Handleable
  self.table_name = 'collection'
  self.primary_key = 'collection_id'
  self.sequence_name = 'collection_seq'

  has_many :owned_items, class_name: 'Item', foreign_key: 'owning_collection'
  belongs_to :template_item, class_name: 'Item', foreign_key: 'template_item_id'
  belongs_to :submitter_group, class_name: 'EPersonGroup', foreign_key: 'submitter'
  belongs_to :admin_group, class_name: 'EPersonGroup', foreign_key: 'admin'
  belongs_to :workflow_step_1_group, class_name: 'EPersonGroup', foreign_key: 'workflow_step_1'
  belongs_to :workflow_step_2_group, class_name: 'EPersonGroup', foreign_key: 'workflow_step_2'
  belongs_to :workflow_step_3_group, class_name: 'EPersonGroup', foreign_key: 'workflow_step_3'
  has_many :collection_item_joins, class_name: 'CollectionItemJoin', foreign_key: 'collection_id'
  has_many :items, through: :collection_item_joins
  has_one :collection_item_count
  has_one :community_collection_joins
  has_one :community, through: :community_collection_joins
  has_many :subscriptions
  has_many :workflows

  def getResourcePolicies
    policies=ResourcePolicy.find_all_by_resource_type_id_and_resource_id(RESOURCE_TYPE['COLLECTION'], self.collection_id)
    return policies
  end
end