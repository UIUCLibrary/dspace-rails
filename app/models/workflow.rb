#I think that this is a better name than suggested by the underlying table name,
#but if not someone can fix it.
#TODO - some sort of interpretation of the 'state' field
class Workflow < DspaceRecord
  self.table_name = 'workflowitem'
  self.primary_key = 'workflow_id'
  self.sequence_name = 'workflowitem_seq'

  belongs_to :item
  belongs_to :collection
  #don't want to use owner here because it is a db field
  belongs_to :my_owner, class_name: 'EPerson', foreign_key: 'owner'
end