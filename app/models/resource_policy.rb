#TODO - it's pretty clear that the resource_type and resource can be mapped (ala handles) and
#the action_id maybe as well, but again I think some of that is DSpace internals that
#will need to be plumbed.
class ResourcePolicy < DspaceRecord

  self.table_name = 'resourcepolicy'
  self.primary_key = 'policy_id'
  self.sequence_name = 'resourcepolicy_seq'

  belongs_to :e_person, class_name: 'EPerson', foreign_key: 'eperson_id'
  belongs_to :e_person_group, class_name: 'EPersonGroup', foreign_key: 'epersongroup_id'

  def policyAlreadyWritten?
    candidate=ResourcePolicy.find_by_resource_type_id_and_resource_id(self.resource_type_id, self.resource_id)
    if candidate and !candidate.rpname.nil? and candidate.rpname==self.rpname and candidate.rptype == self.rptype and candidate.epersongroup_id == self.epersongroup_id and candidate.start_date == self.start_date
      return true
    end
  end

end