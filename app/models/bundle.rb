class Bundle < DspaceRecord
  self.table_name = 'bundle'
  self.primary_key = 'bundle_id'
  self.sequence_name = 'bundle_seq'

  belongs_to :primary_bitstream, class_name: 'Bitstream', foreign_key: 'primary_bitstream_id'
  has_one :item_bundle_join
  has_one :item, through: :item_bundle_join
  has_many :bundle_bitstream_joins
  has_many :bitstreams, through: :bundle_bitstream_joins

  def getBitstreamIds
    id_array=Array.new
    self.bitstreams.each do |i|
      id_array<<i.bitstream_id
    end
    return id_array
  end
end