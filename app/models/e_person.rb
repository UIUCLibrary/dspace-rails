require 'lib/uiuc_ldap'

class EPerson < DspaceRecord
  self.table_name = 'eperson'
  self.primary_key = 'eperson_id'
  self.sequence_name = 'eperson_seq'

  has_many :submissions, class_name: 'Item', foreign_key: 'submitter_id'
  has_many :e_person_e_person_group_joins, class_name: 'EPersonEPersonGroupJoin', foreign_key: 'eperson_id'
  has_many :e_person_groups, through: :e_person_e_person_group_joins
  has_many :resource_policies, foreign_key: 'eperson_id'
  has_many :subscriptions, foreign_key: 'eperson_id'
  has_many :workflows, foreign_key: 'owner'

  #Given a list of netids, create a new eperson if necessary, lookup email and first/last name, and set default
  #attribtes for each as passed. E.g. to allow them all to log in {:can_log_in => true}
  def self.add_all(netid_list, default_attributes = {})
    EPerson.transaction do
      netid_list.each do |netid|
        ldap_information = UiucLdap.person_record(netid)
        #look up names and email via ldap and update
        email = ldap_information.at_css('attr[name="mail"] value').text
        firstname = ldap_information.at_css('attr[name="givenname"] value').text
        lastname = ldap_information.at_css('attr[name="sn"] value').text
        eperson = EPerson.where(email: email).first
        unless eperson
          eperson = EPerson.new
          eperson.email = email
        end
        default_attributes.each { |k, v| eperson[k] = v }
        eperson.netid = netid
        eperson.firstname = firstname
        eperson.lastname = lastname
        puts eperson.attributes
        eperson.save!
      end
    end
  end

end