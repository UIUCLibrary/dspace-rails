class EPersonEPersonGroupJoin < DspaceRecord
  self.table_name = 'epersongroup2eperson'
  self.sequence_name = 'epersongroup2eperson_seq'
  self.primary_key = 'id'

  belongs_to :e_person, class_name: 'EPerson', foreign_key: 'eperson_id'
  belongs_to :e_person_group, class_name: 'EPersonGroup', foreign_key: 'eperson_group_id'

end