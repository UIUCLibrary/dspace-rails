class MetadataSchema < DspaceRecord
  self.table_name = 'metadataschemaregistry'
  self.primary_key = 'metadata_schema_id'
  self.sequence_name = 'metadataschemaregistry_seq'

  has_many :metadata_fields
end