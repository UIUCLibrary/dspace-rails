class RestrictItem < DspaceRecord
  self.table_name = 'restrict_item'
  self.primary_key = 'id'
  self.sequence_name = 'restrict_item_seq'

  belongs_to :item
  belongs_to :e_person_group

  # To change this template use File | Settings | File Templates.
end