class BiWithdrawn < DspaceRecord
  self.table_name = 'bi_withdrawn'
  self.primary_key = 'id'
  self.sequence_name = 'bi_withdrawn_seq'

  belongs_to :item
end