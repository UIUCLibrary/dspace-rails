class HarvestedItem < DspaceRecord
  self.table_name = 'harvested_item'
  self.primary_key = 'id'
  self.sequence_name = 'harvested_item_seq'

  belongs_to :item

end