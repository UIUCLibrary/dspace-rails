class Subscription < DspaceRecord
  self.table_name = 'subscription'
  self.primary_key = 'subscription_id'
  self.sequence_name = 'subscription_seq'

  belongs_to :e_person, class_name: 'EPerson', foreign_key: 'eperson_id'
  belongs_to :collection

end