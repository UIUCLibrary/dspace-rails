# TODO this will need some fleshing out. I think that the resource_type_id is hardcoded (I don't see
# a table for it), It appears to be:
# 2 - item
# 3 - collection
# 4 - community
#According to the docs these are the only things that get handles. I don't know where '1' might
#have gone.
class Handle < DspaceRecord
  self.table_name = 'handle'
  self.primary_key = 'handle_id'
  self.sequence_name = 'handle_seq'

  #return the resource associated with this handle object
  def resource
    klass = HandleHelper.resource_type_id_to_class(self.resource_type_id)
    klass.find(self.resource_id)
  end

  #find the resource with the given handle string
  def self.lookup_resource(handle_string)
    self.where(:handle => handle_string).first.try(:resource)
  end

end