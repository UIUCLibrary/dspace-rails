#This class is over a view, so you shouldn't try to modify or create. Outbound associations are
#provided, but not inbound.
#This may provide small gains in efficiency or convenience, but it should not be much over
#properly constructed AREL. Since this aggregates three tables it may be a bit more than
#CommunityCommunityJoin, though.
class DcValue < ActiveRecord::Base
  self.table_name = 'dcvalue'

  belongs_to :metadata_value, foreign_key: 'dc_value_id'
  belongs_to :metadata_field, foreign_key: 'dc_type_id'
  belongs_to :item
end