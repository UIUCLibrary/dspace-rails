module Handleable
  def handle_object
    Handle.where(:resource_id => self.id, :resource_type_id => HandleHelper.class_to_resource_type_id(self.class)).first
  end

  def handle
    self.handle_object.handle
  end

end