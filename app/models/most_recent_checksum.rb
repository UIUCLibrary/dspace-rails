class MostRecentChecksum < DspaceRecord
  self.table_name = 'most_recent_checksum'
  self.primary_key = 'bitstream_id'

  belongs_to :bitstream
  belongs_to :checksum_result

end