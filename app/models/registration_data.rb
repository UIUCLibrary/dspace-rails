class RegistrationData < DspaceRecord
  self.table_name = 'registrationdata'
  self.primary_key = 'registrationdata_id'
  self.sequence_name = 'registrationdata_seq'

end