class WorkspaceItem < DspaceRecord
  self.table_name = 'workspaceitem'
  self.primary_key = 'workspace_item_id'
  self.sequence_name = 'workspaceitem_seq'

  belongs_to :item
  belongs_to :collection
  has_many :e_person_group_workspace_item_joins
  has_many :e_person_groups, through: :e_person_group_workspace_item_joins
end