class Bi2Dmap < DspaceRecord
  self.table_name = 'bi_2_dmap'
  self.primary_key = 'map_id'
  self.sequence_name = 'bi_2_dmap_seq'

  belongs_to :item
  belongs_to :bi2_dis, foreign_key: 'distinct_id'
end