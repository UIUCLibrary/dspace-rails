class HarvestedCollection < DspaceRecord
  self.table_name = 'harvested_collection'
  self.primary_key = 'id'
  self.sequence_name = 'harvested_collection_seq'

  belongs_to :collection
end