class BitstreamFormat < DspaceRecord
  self.table_name = 'bitstreamformatregistry'
  self.primary_key = 'bitstream_format_id'
  self.sequence_name = 'bitstreamformatregistry_seq'

  has_many :bitstreams
  has_many :file_extensions

end