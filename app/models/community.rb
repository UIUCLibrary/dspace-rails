class Community < DspaceRecord
  include Handleable
  self.table_name = 'community'
  self.primary_key = 'community_id'
  self.sequence_name = 'community_seq'

  belongs_to :logo_bitstream, class_name: 'Bitstream', foreign_key: 'logo_bitstream_id'
  belongs_to :admin_group, class_name: 'EPersonGroup', foreign_key: 'admin'

  has_many :child_community_joins, class_name: 'CommunityCommunityJoin', foreign_key: 'parent_comm_id'
  has_many :child_communities, through: :child_community_joins
  has_one :parent_community_join, class_name: 'CommunityCommunityJoin', foreign_key: 'child_comm_id'
  has_one :parent_community, through: :parent_community_join
  has_one :community_item_count
  has_many :community_collection_joins
  has_many :collections, through: :community_collection_joins



end