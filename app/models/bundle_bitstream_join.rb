class BundleBitstreamJoin < DspaceRecord
  self.table_name = 'bundle2bitstream'
  self.primary_key = 'id'
  self.sequence_name = 'bundle2bitstream_seq'

  belongs_to :bundle
  belongs_to :bitstream

end