class ChecksumHistory < DspaceRecord
  self.table_name = 'checksum_history'
  self.primary_key = 'check_id'
  self.sequence_name = 'checksum_history_check_id_seq'

  belongs_to :bitstream
  belongs_to :checksum_result, :foreign_key => 'result'

end