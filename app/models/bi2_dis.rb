class Bi2Dis < DspaceRecord
  self.table_name = 'bi_2_dis'
  self.primary_key = 'id'
  self.sequence_name = 'bi_2_dis_seq'

  has_many :bi2_dmaps, foreign_key: 'distinct_id'
end