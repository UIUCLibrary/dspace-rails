class CommunityItemCount < DspaceRecord
  self.table_name = 'community_item_count'
  self.primary_key = 'community_id'

  belongs_to :community

end