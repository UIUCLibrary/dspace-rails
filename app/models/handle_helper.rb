module HandleHelper
  module_function

  TYPE_ID_TO_CLASS_MAP = {2 => Item, 3 => Collection, 4 => Community}
  CLASS_TO_TYPE_ID_MAP = TYPE_ID_TO_CLASS_MAP.invert

  def resource_type_id_to_class(id)
    TYPE_ID_TO_CLASS_MAP[id] or raise RuntimeError, "Unrecognized resource_type_id"
  end

  def class_to_resource_type_id(klass)
    CLASS_TO_TYPE_ID_MAP[klass] or raise RuntimeError, "Unrecognize class #{klass.to_s} for handle"
  end

end