class EPersonGroupWorkspaceItemJoin < DspaceRecord
  self.table_name = 'epersongroup2workspaceitem'
  self.primary_key = 'id'
  self.sequence_name = 'epersongroup2workspaceitem_seq'

  belongs_to :e_person_group
  belongs_to :workspace_item

end