class Bi4Dis < DspaceRecord
  self.table_name = 'bi_4_dis'
  self.primary_key = 'id'
  self.sequence_name = 'bi_4_dis_seq'

  has_many :bi4_dmaps, foreign_key: 'distinct_id'
end