class CollectionItemCount < DspaceRecord
  self.table_name = 'collection_item_count'
  self.primary_key = 'collection_id'

  belongs_to :collection

end