require 'net/http'

module UiucLdap
  class LDAPError < RuntimeError;
  end;

  module_function

  LDAP_LOOKUP_BASE_URL = 'http://quest.grainger.uiuc.edu/directory'

  def person_record(net_id)
    url_string = "#{LDAP_LOOKUP_BASE_URL}/ad/person/#{net_id}"
    url = URI.parse(url_string)
    request = Net::HTTP::Get.new(url.path)
    response = Net::HTTP.start(url.host, url.port) do |http|
      http.request(request)
    end
    if response.class == Net::HTTPOK
      Nokogiri::XML::Document.parse(response.body)
    else
      raise LDAPError, "Could not look up net id #{net_id}"
    end

  end

end