namespace :generate_report do
  desc 'generate a qc report after the embargo system migration has run'
  task :post_migration => :environment do
    RestrictItem.all.each do |restrict_item|
      puts "\nBegin Item "+restrict_item.item_id.to_s
      test_item_level_metadata(restrict_item)
      test_bitstream_level_policies(restrict_item)
    end
  end
end

def test_bitstream_level_policies(restrict_item)
  puts "Testing Item Level Policies for "+restrict_item.item_id.to_s
  item = Item.find(restrict_item.item_id)
  bundle_ids=item.getBundleIds
  bundle_ids.each do |bundle_id|


    bitstream_ids = get_bundle_bitstreams(bundle_id)
    if restrict_item.eperson_group_id.nil?
      test_full_embargo_bitstreams(restrict_item, bitstream_ids)
    elsif restrict_item.eperson_group_id==2
      test_ui_only_bitstreams(restrict_item, bitstream_ids)
    elsif restrict_item.eperson_group_id==1 or restrict_item.eperson_group_id > 2
      test_restrict_bitstreams(restrict_item, bitstream_ids)
    end
  end
end

def test_item_level_metadata(restrict_item)
  if restrict_item.eperson_group_id.nil?
    puts "Item is fully embargoed"
    check_metadata_fields(restrict_item)
  elsif restrict_item.eperson_group_id==2
    puts "Item is ui only"
    check_metadata_fields(restrict_item)
  elsif restrict_item.eperson_group_id==1 or restrict_item.eperson_group_id > 2
    puts "Item is restricted"
    check_metadata_fields(restrict_item)
  else
    write_problem_message "Item "+restrict_item.item_id+" has unrecognizable restriction type."
  end
end

def get_bundle_bitstreams(bundle_id)
  bundle=Bundle.find_by_bundle_id(bundle_id)
  return bundle.getBitstreamIds
end

def get_bitstream_ids_for_item(bundle_ids)
  bitstream_ids=Array.new
  bundle_ids.each do |bundle_id|
    bundle=Bundle.find_by_bundle_id(bundle_id)
    bundle.getBitstreamIds.each do |bitstream_id|
      bitstream_ids<<bitstream_id
    end
  end
  return bitstream_ids
end

def test_ui_only_bitstreams(restrict_item, nonlicense_bitstreams_ids)
  nonlicense_bitstreams_ids.each do |bitstream_id|
    potential_problem=0
    puts "bitstream: "+bitstream_id.to_s
    rp_array = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_rptype(RESOURCE_TYPE["BITSTREAM"], bitstream_id, 'TYPE_CUSTOM')
    release_policy_array = rp_array.select do |rp|
      rp.epersongroup_id==0
    end
    restrict_policy_array = rp_array.select do |rp|
      rp.epersongroup_id==2
    end

    #test for correct number of release policies
    if release_policy_array.nil?
      potential_problem+=1
      write_problem_message "release policy not found"
    elsif release_policy_array.length>1
      potential_problem+=1
      write_problem_message "release policy array length: "+ release_policy_array.length.to_s
    else
      release_policy = release_policy_array[0]
    end

    #test for correct number of restrict policies
    if restrict_policy_array.nil?
      potential_problem+=1
      write_problem_message "restrict policy not found"
    elsif restrict_policy_array.length>1
      potential_problem+=1
      write_problem_message "restrict policy array length: "+ restrict_policy_array.length.to_s
    else
      restrict_policy = restrict_policy_array[0]
    end

    #release policy specific tests
    if release_policy
      if !bitstream_date_match_restriction?(release_policy, restrict_item)
        potential_problem+=1
        write_problem_message "rp startdate is "+release_policy.start_date.to_s+" while restriction release date is "+release_date_or_empty(restrict_item)
      end
    end

    #restrict policy specific tests
    if restrict_policy
      if restrict_policy.epersongroup_id!=2
        potential_problem+=1
        write_problem_message( "number rp group is "+rp.epersongroup_id.to_s)
      end
    end

    if potential_problem>0
      dump_all_policy_info(rp_array)
    end
  end
end

def test_restrict_bitstreams(restrict_item, nonlicense_bitstreams_ids)
  nonlicense_bitstreams_ids.each do |bitstream_id|
    potential_problem=0
    puts "bitstream: "+bitstream_id.to_s
    rp_array = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_rptype(RESOURCE_TYPE["BITSTREAM"], bitstream_id, 'TYPE_CUSTOM')
    release_policy_array = rp_array.select do |rp|
      rp.epersongroup_id==0
    end
    restrict_policy_array = rp_array.select do |rp|
      rp.epersongroup_id==1 or rp.epersongroup_id>2
    end

    #test for correct number of release policies
    if release_policy_array.nil?
      potential_problem+=1
      write_problem_message "release policy not found"
    elsif release_policy_array.length>1
      potential_problem+=1
      write_problem_message "release policy array length: "+ release_policy_array.length.to_s
    else
      release_policy = release_policy_array[0]
    end

    #test for correct number of restrict policies
    if restrict_policy_array.nil?
      potential_problem+=1
      write_problem_message "restrict policy not found"
    elsif restrict_policy_array.length>1
      potential_problem+=1
      write_problem_message "restrict policy array length: "+ restrict_policy_array.length.to_s
    else
      restrict_policy = restrict_policy_array[0]
    end

    #release policy specific tests
    if release_policy
      if !bitstream_date_match_restriction?(release_policy, restrict_item)
        potential_problem+=1
        write_problem_message "release policy startdate is "+release_policy.start_date.to_s+" while restriction release date is "+restrict_item.release_date.to_s
      end
    end

    #restrict policy specific tests
    if restrict_policy
      if restrict_policy.epersongroup_id!=1
        potential_problem+=1
        write_problem_message( "number rp group is "+restrict_policy.epersongroup_id.to_s)
      end
    end

    if potential_problem>0
      dump_all_policy_info(rp_array)
    end
  end
end

def test_full_embargo_bitstreams(restrict_item, nonlicense_bitstreams_ids)
  nonlicense_bitstreams_ids.each do |bitstream_id|
    puts "bitstream: "+bitstream_id.to_s
    rp_array = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_rptype(RESOURCE_TYPE["BITSTREAM"], bitstream_id, 'TYPE_CUSTOM')
    if rp_array.length==1
      puts "number of custom rps is 1"
    else
      write_problem_message("number of custom rps is "+rp_array.length.to_s)
    end
    rp_array.each do |rp|
      puts "rp: "+rp.policy_id.to_s
      if rp.epersongroup_id!=0
        write_problem_message( "number rp group is "+rp.epersongroup_id.to_s)
      end
      if !bitstream_date_match_restriction?(rp, restrict_item)
        write_problem_message("rp startdate is "+rp.start_date.to_s+" while restriction release date is "+restrict_item.release_date.to_s)
      end
    end
  end
end


def write_problem_message(message)
  puts "********************************************POTENTIAL PROBLEM**************************************"
  puts message
  puts "****************************************************************************************************"
end

def dump_all_policy_info(rp_array)
  rp_array.each do |rp|
    puts "rp: "+rp.policy_id.to_s
    puts "resource_type_id "+rp.resource_type_id.to_s
    puts "resource_id "+rp.resource_id.to_s
    puts "action_id "+rp.action_id.to_s
    if !rp.eperson_id.nil?
      puts "eperson_id "+rp.eperson_id.to_s
    end
    if !rp.epersongroup_id.nil?
      puts "epersongroup_id "+rp.epersongroup_id.to_s
    end
    if !rp.start_date.nil?
      puts "start_date "+ rp.start_date.to_s
    end
    if !rp.rpname.nil?
      puts "rpname "+ rp.rpname.to_s
    end
    if !rp.rptype.nil?
      puts "rptype "+ rp.rptype.to_s
    end
    if !rp.rpdescription.nil?
      puts "rpdescription "+ rp.rpdescription.to_s
    end
  end
end

def check_metadata_fields(restrict_item)
  check_item_terms(restrict_item)
  check_item_date(restrict_item)
  check_item_reason(restrict_item)
end


def check_item_terms(restrict_item)
  termsfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'terms')
  mv_array = MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, termsfield.metadata_field_id)
  if mv_array.nil? or mv_array.length==0
    write_problem_message("no global terms found")
  else
    mv_unique = mv_array.uniq
    if mv_unique.length>1
      write_problem_message "global terms conflict"
    else
      terms=mv_unique[0]
    end
  end
  if terms
    if terms.text_value!=resolve_terms(restrict_item.eperson_group_id)
      write_problem_message "global terms "+terms.text_value.to_s+" conflicts with restriction group "+restrict_item.eperson_group_id.to_s
    end
  end
end

def check_item_date(restrict_item)
  liftdatefield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'date', 'embargo')
  mv_array = MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, liftdatefield.metadata_field_id)
  if mv_array.nil? or mv_array.length==0
    write_problem_message("no global date found")
  else
    mv_unique = mv_array.uniq
    if mv_unique.length>1
      write_problem_message "global dates conflict"
    else
      date=mv_unique[0]
    end
  end
  if date
    if !global_date_match_restriction?(date.text_value, restrict_item)
      write_problem_message "global date "+date.text_value.to_s+" conflicts with restriction date"+restrict_item.release_date.to_s
    end
  end
end

def check_item_reason(restrict_item)
  reasonfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'reason')
  mv_array = MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, reasonfield.metadata_field_id)
  if mv_array.nil? or mv_array.length==0
    write_problem_message("no global reason found")
  else
    reason_array = Array.new
    mv_array.each do |mv|
      reason_array << mv.text_value
    end
    unique_reasons = reason_array.uniq
    if unique_reasons.length>1
      write_problem_message "global reason conflict. unique reasons: "+unique_reasons.to_s
    else
      reason=unique_reasons[0]
    end
  end
  if reason && restrict_item.item_id != 35082
    if reason!=truncate(restrict_item.reason, 99)
      write_problem_message "global reason "+reason+" conflicts with restriction reason "+restrict_item.reason.to_s
    end
  end
end


def resolve_terms(group_id)
  if group_id==2
    return "U of I Only"
  elsif group_id==nil
    return "Embargoed"
  else
    return "Limited"
  end
end

def release_date_or_empty(restrict_item)
  if restrict_item.release_date.nil?
    return "empty"
  end
  return restrict_item.release_date.to_s
end

def bitstream_date_match_restriction?(resource_policy, restrict_item)
  if resource_policy.start_date.nil?
    policy_string = "empty"
  elsif resource_policy.start_date.to_s[0..4]=="10000"
    policy_string = "forever"
  else
    policy_string = resource_policy.start_date.to_s[0..9]
  end
  if restrict_item.release_date.nil?
    restriction_string = "forever"
  else
    restriction_string = restrict_item.release_date.to_s[0..9]
  end
  return policy_string == restriction_string

end

def global_date_match_restriction?(global_date_val, restrict_item)
  if global_date_val.nil?
    global_string = "empty"
  elsif global_date_val.to_s[0..4]=="10000"
    global_string = "forever"
  else
    global_string = global_date_val.to_s[0..9]
  end
  if restrict_item.release_date.nil?
    restriction_string = "forever"
  else
    restriction_string = restrict_item.release_date.to_s[0..9]
  end
  return global_string == restriction_string

end