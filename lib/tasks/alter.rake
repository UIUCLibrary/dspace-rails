require "rake"
require_relative '../excel_reader'

namespace :alter do

  desc 'add item in collection without deleting from original'
  task :remap_item => :environment do
    item_id=Handle.lookup_resource(ENV['C_HANDLE']).item_id
    col_id=Handle.lookup_resource(ENV['I_HANDLE']).collection_id
    remap(item_id, col_id)
  end

  desc 'remap batch of items based on excel file'
  task :remap_batch => :environment do
    f=ENV['FILE']
    csv=ExcelReader.new(f)
    csv.select_header_row do
      true
    end
    csv.content_hashes.each do |row|
      item_id=row["item_handle" ]
      col_ids=row["new collection handles"].split(', ')

      col_ids.each do |col_id|
        add_item_to_collection(item_id, col_id)
      end

    end
  end

  desc 'test excel file'
  task :csv_test => :environment do
    f=ENV['FILE']
    csv=ExcelReader.new(f)
    csv.select_header_row do
      true
    end
    puts csv.content_hashes[1]
  end
end



def find_metadata_value_id(row)
  #row.each do |column, value|
  # filter = {:dc => [/dc\..*/, 1], :thesis => [/thesis\..*/, 3], :mods => [mods\..*/, 4]}
  # filter.each do |filter|
  #   if column == filter do
  #
  # end
end

def change_metadata_value()

end

def remove_item_from_collection(item_id, collection_id)

end

def change_owning_collection(item_id, collection_id)

end


