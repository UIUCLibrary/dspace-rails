require 'yaml'
namespace :medusa do

  desc 'Tasks for connecting Ideals to Medusa'
  task :export => :environment do
    communities = getTopLevelCommunities
    communities.each do |community|
      puts community.community_id.to_s+"\t| "+community.name
      community_handle = Handle.find_all_by_resource_id_and_resource_type_id(community.community_id, 4)
      export_one_community(community_handle[0].handle)
    end
  end
  desc 'Export one Community'
  task :export_one, [:community_id] => :environment do |t, community_id|
    community_handle = Handle.find_all_by_resource_id_and_resource_type_id(community_id[:community_id], 4)
    export_one_community(community_handle[0].handle)
  end
  desc 'Show the export commands without running them'
  task :export_test_one, [:community_id] => :environment do |t, community_id|
    community_handle = Handle.find_all_by_resource_id_and_resource_type_id(community_id[:community_id], 4)
    test_one_community(community_handle[0].handle)
  end
  desc 'Tasks for connecting Ideals to Medusa'
  task :export_test => :environment do
    communities = getTopLevelCommunities
    communities.each do |community|
      community_handle = Handle.find_all_by_resource_id_and_resource_type_id(community.community_id, 4)
      test_one_community(community_handle[0].handle)
    end
  end
end

def getTopLevelCommunities
  topLevelCommunities = Community.all.reject do |community|
    community.parent_community
  end
end

def export_one_community(community_handle)
  data = YAML.load_file "config/dspace.yml"
  export_args = data["export_args"]
  smbclient_args = data['smbclient_args']
  export_directory_name = construct_export_directory_name community_handle

  system("mkdir #{export_args['output_dir']}")
  system("mkdir #{export_args['output_dir']}/#{export_directory_name}-export")

  system("#{export_args['dspace_launcher']} -d -u -a -t MEDUSA -e #{export_args['eperson']} -i #{community_handle} #{export_args['output_dir']}/#{export_directory_name}-export/#{export_directory_name}")
  system("smbclient #{smbclient_args['destination_host']} --authentication-file #{smbclient_args['auth_file']} -c \"cd #{smbclient_args['destination_dir']}; lcd #{export_args['output_dir']};prompt;recurse;mput *;exit\"")
  system("rm -rf #{export_args['output_dir']}")
end

def construct_export_directory_name(community_handle)
  handle_components = community_handle.split('/')
  export_directory_name = "COMMUNITY@#{handle_components[0]}-#{handle_components[1]}"
end

def test_one_community(community_handle)
  data = YAML.load_file "config/dspace.yml"
  export_args = data["export_args"]
  smbclient_args = data['smbclient_args']
  export_directory_name = construct_export_directory_name community_handle

  puts "mkdir #{export_args['output_dir']}"
  puts "mkdir #{export_args['output_dir']}/#{export_directory_name}-export"

  puts "#{export_args['dspace_launcher']} -d -u -a -t MEDUSA -e #{export_args['eperson']} -i #{community_handle} #{export_args['output_dir']}/#{export_directory_name}-export/#{export_directory_name}"
  puts "smbclient #{smbclient_args['destination_host']} --authentication-file #{smbclient_args['auth_file']} -c \"cd #{smbclient_args['destination_dir']}; lcd #{export_args['output_dir']};prompt;recurse;mput *;exit\""
  puts "rm -rf #{export_args['output_dir']}"
end


def directory_exists?(directory)
  File.directory?(directory)
end