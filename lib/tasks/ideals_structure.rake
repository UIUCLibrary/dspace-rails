namespace :generate_report do

  desc 'generate a listing of all communities, subcommunities, and collections'
  task :ideals_structure => :environment do
    string = %q{
  **********************************
  *         IDEALS                 *
  * Community/Collection Structure *
  **********************************
}
    puts string
    top_level_communities = sort_communities(filter_top_level_communities)
    top_level_communities.each do |comm|
      write_tree(comm, sort_communities(comm.child_communities), 0)
    end
  end
end

def sort_communities(comm_array)
  comm_array.sort! {|a,b| a.name <=> b.name}
end

def filter_top_level_communities
  non_toplevel_ids = get_nontoplevel_ids
  communities = Array.new
  Community.all.reject! {|comm| non_toplevel_ids.include? comm.community_id}
end

def get_nontoplevel_ids
  ids = Array.new
  CommunityCommunityJoin.all.each do |c2c|
     ids << c2c.child_comm_id
  end
  ids.uniq
end

def write_tree(comm, child_communities, depth)
  out=String.new
  depth.times {out+="\t"}

  puts out+comm.name
  collections = sort_communities(comm.collections)
  collections.each do |coll|
    puts out+"\tCollection: "+coll.name
  end
  if child_communities==[]
    return
  else
    comm.child_communities.each do |comm|
      write_tree(comm, sort_communities(comm.child_communities), depth+1)
    end
  end



end