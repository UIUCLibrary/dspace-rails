
namespace :export do
  desc 'populate data in the 3.1 embargo system with data from the 1.5 restriction system'
  task :embargo => :environment do
    RestrictItem.all.each do |restrict_item|
      #this particular row has bad data
      if restrict_item.item_id!=35082
        puts "processing restriction for item "+restrict_item.item_id.to_s
        item=Item.find(restrict_item.item_id)
        if restrict_item.eperson_group_id.nil?
          delete_anonymous_policies(restrict_item)
          create_embargo_release_policies(RESOURCE_TYPE['ITEM'], restrict_item.item_id, restrict_item)
          dedupe_custom_policies_for_dso(RESOURCE_TYPE['ITEM'], restrict_item.item_id, restrict_item)
        end
        create_policies_for_item_subcomponents(item, restrict_item)
        if restrict_item.eperson_group_id.nil? and item.owning_collection
          item.makePrivate
          item.save
        end
        create_provenance restrict_item
        remove_terms restrict_item
        add_terms restrict_item
        remove_date restrict_item
        add_date_metadata restrict_item
        remove_reason restrict_item
        add_reason_metadata restrict_item
      end
    end
  end

  task :add_globalinfo => :environment do
    RestrictItem.all.each do |i|
      add_terms i
      add_date_metadata i
      add_reason_metadata i
    end
  end

  task :delete_unattachedMetadatavalues => :environment do
    MetadataValue.find_all_by_item_id(nil).each do |mv|
      mv.destroy
    end
  end

  task :delete_duplicate_custom_policies => :environment do
    RestrictItem.all.each do |restrict_item|
        item=Item.find(restrict_item.item_id)
        dedupe_custom_policies_for_dso(RESOURCE_TYPE['ITEM'], item.item_id, restrict_item)
        bundles=item.getBundleIds
        bundles.each do |bundle_id|
          bundle = Bundle.find(bundle_id)
          dedupe_custom_policies_for_dso(RESOURCE_TYPE['BUNDLE'], bundle_id, restrict_item)
          bitstreams=bundle.getBitstreamIds
          bitstreams.each do |bitstream_id|
            dedupe_custom_policies_for_dso(RESOURCE_TYPE['BITSTREAM'], bitstream_id, restrict_item)
          end
        end
      end
  end

  task :show_unattachedMetadatavalues => :environment do
    MetadataValue.find_all_by_item_id(nil).each do |mv|
      mv.text_value
    end
  end


  task :fixGlobalTerms  => :environment do
    RestrictItem.all.each do |i|
      remove_terms i
      add_terms i
      remove_date i
      add_date_metadata i
      remove_reason i
      add_reason_metadata i
    end
  end
end




def create_policies_for_item_subcomponents(item, restrict_item)
  bundles=item.getBundleIds
  bundles.each do |id|
    bundle=Bundle.find_by_bundle_id(id)
    create_embargo_release_policies(RESOURCE_TYPE['BUNDLE'],id, restrict_item)
    if !restrict_item.eperson_group_id.nil?
      alter_or_create_restrict_policies(RESOURCE_TYPE['BUNDLE'], id, restrict_item.eperson_group_id, restrict_item)
    end
    bitstreams=bundle.getBitstreamIds
    bitstreams.each do |bid|
      create_embargo_release_policies(RESOURCE_TYPE['BITSTREAM'],bid, restrict_item)
      if !restrict_item.eperson_group_id.nil?
        alter_or_create_restrict_policies(RESOURCE_TYPE['BITSTREAM'], bid, restrict_item.eperson_group_id, restrict_item)
      end
      dedupe_custom_policies_for_dso(RESOURCE_TYPE['BITSTREAM'],bid, restrict_item)
    end
    dedupe_custom_policies_for_dso(RESOURCE_TYPE['BUNDLE'],id, restrict_item)
  end
end

def delete_anonymous_policies(restrict_item)
  if restrict_item.eperson_group_id.nil?
    item_rps = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_action_id_and_epersongroup_id(RESOURCE_TYPE['ITEM'], restrict_item.item_id, ACTION['READ'], 0)
    item_rps.each do |rp|
      rp.destroy
    end
    item=Item.find(restrict_item.item_id)
    bundle_ids=item.getBundleIds
    bundle_ids.each do |bundle_id|
      bundle_rps = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_action_id_and_epersongroup_id(RESOURCE_TYPE['BUNDLE'], bundle_id, ACTION['READ'], 0)
      bundle_rps.each do |rp|
        rp.destroy
      end
      bundle=Bundle.find_by_bundle_id(bundle_id)
      bitstream_ids = bundle.getBitstreamIds
      bitstream_ids.each do |bitstream_id|
        bitstream_rps = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_action_id_and_epersongroup_id(RESOURCE_TYPE['BITSTREAM'], bitstream_id, ACTION['READ'], 0)
        bitstream_rps.each do |rp|
          rp.destroy
        end
      end
    end
  end
end

def create_embargo_release_policies(type, resource_id, restrict_item)
  getEmbargoGroup(restrict_item).each do |group_id|
    rp=createEmbargoReleasePolicy(type, resource_id, restrict_item, group_id)
    if !rp.policyAlreadyWritten?
      rp.save
    else
      puts "duplicate found for the following item, item skipped:\nitem: "+restrict_item.item_id.to_s+" \nresource type: "+type.to_s+"\nid: "+resource_id.to_s+"\n"
    end
  end
end

def createEmbargoReleasePolicy(type, resource_id, restrict_item, group)
  rp=ResourcePolicy.new
  rp.resource_type_id=type
  rp.resource_id=resource_id
  rp.action_id=ACTION['READ']
  rp.epersongroup_id=group
  if restrict_item.release_date.nil?
    rp.start_date = "10000-01-01".to_date
  else
    rp.start_date=restrict_item.release_date
  end
  rp.rptype='TYPE_CUSTOM'
  rp.rpname="From 1.5.2 restrict_item "+restrict_item.id.to_s
  rp.rpdescription=truncate(restrict_item.reason, 99)
  return rp
end

def getEmbargoGroup(restrict_item)
  item=Item.find(restrict_item.item_id)
  group_id_array=[0]
  if item.owning_collection
    owning_collection=Collection.find(item.owning_collection)
    oc_policies=ResourcePolicy.find_all_by_resource_type_id_and_resource_id(RESOURCE_TYPE['COLLECTION'], owning_collection.collection_id)
    group_id_array=resolveCollectionPolicies(oc_policies)
  end
  return group_id_array
end

def resolveCollectionPolicies(rp_array)
  group_array=Array.new
  rp_array.each do |i|
    if i.action_id==ACTION['READ']
      #doing it this way to accommodate future situations wherein an embargo lifts to a non-anonymous group
      if i.epersongroup_id==0
        group_array=Array.new
        group_array<<i.epersongroup_id
        return group_array
      else
        group_array<<i.epersongroup_id
      end
    end
  end
  return group_array.uniq
end

def truncate(str, len)
  if str.length > len then return str[0,len] else return str end
end

def build_provenance(restrict_item)
  return "Restriction data tranferred "+DateTime.now.to_s +
    "\nOriginal Data" +
    "\nGroup with Access " +
    if restrict_item.eperson_group_id.nil? then "full embargo (none)" else EPersonGroup.find(restrict_item.eperson_group_id).name end +
    "\nRelease Date: " +
    if restrict_item.release_date.nil? then "none" else restrict_item.release_date.to_s end +
    "\nReason: " +
    restrict_item.reason
end

def create_provenance(restrict_item)
  mv=MetadataValue.new
  mv.item_id=restrict_item.item_id
  mv.metadata_field_id=28
  mv.text_value=build_provenance(restrict_item)
  mv.place=3
  mv.text_lang="en"
  mv.save
end

def remove_terms(restrict_item)
  termsfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'terms')
  MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, termsfield.metadata_field_id).each do |mv|
    mv.destroy
  end
end

def remove_date(restrict_item)
    liftdatefield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'date', 'embargo')
    MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, liftdatefield.metadata_field_id).each do |mv|
      mv.destroy
    end
end

def remove_reason(restrict_item)
  reasonfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'reason')
  MetadataValue.find_all_by_item_id_and_metadata_field_id(restrict_item.item_id, reasonfield.metadata_field_id).each do |mv|
    mv.destroy
  end
end

def add_terms(restrict_item)
  mv=MetadataValue.new
  termsfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'terms')
  mv.item_id=restrict_item.item_id
  mv.metadata_field_id=termsfield.metadata_field_id
  mv.text_value=resolve_terms(restrict_item.eperson_group_id)
  mv.place=1
  mv.save
end

def add_date_metadata(restrict_item)
  mv=MetadataValue.new
  liftdatefield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'date', 'embargo')
  mv.item_id=restrict_item.item_id
  mv.metadata_field_id=liftdatefield.metadata_field_id
  if restrict_item.release_date.nil?
    mv.text_value = '10000-01-01'
  else
    mv.text_value=restrict_item.release_date
  end
  mv.place=1
  mv.save
end

def add_reason_metadata(restrict_item)
  mv=MetadataValue.new
  reasonfield=MetadataField.find_by_metadata_schema_id_and_element_and_qualifier(1, 'description', 'reason')
  mv.item_id=restrict_item.item_id
  mv.metadata_field_id=reasonfield.metadata_field_id
  mv.text_value=restrict_item.reason
  mv.place=1
  mv.save
end

def resolve_terms(group_id)
  if group_id==2
    return "U of I Only"
  elsif group_id==nil
    return "Embargoed"
  else
    return "Limited"
  end
end

def alter_or_create_restrict_policies(resource_type, resource_id, group_id, restrict_item)
  rp = ResourcePolicy.find_by_resource_type_id_and_resource_id_and_action_id_and_epersongroup_id(resource_type, resource_id, ACTION['READ'], group_id)
  if rp.nil?
     rp = create_embargo_restrict_policy(resource_type, resource_id, restrict_item, group_id)
     #puts "about to save "+rp.policy_id.to_s+"    created from scratch"
     rp.save
  else
    rp.rptype='TYPE_CUSTOM'
    rp.rpname="From 1.5.2 restrict_item "+restrict_item.id.to_s
    rp.rpdescription=truncate(restrict_item.reason, 99)
    #puts "about to save "+rp.policy_id.to_s
    rp.save
  end
end

def create_embargo_restrict_policy(type, resource_id, restrict_item, group)
  rp=ResourcePolicy.new
  rp.resource_type_id=type
  rp.resource_id=resource_id
  rp.action_id=ACTION['READ']
  rp.epersongroup_id=group
  rp.rptype='TYPE_CUSTOM'
  rp.rpname="From 1.5.2 restrict_item "+restrict_item.id.to_s
  rp.rpdescription=truncate(restrict_item.reason, 99)
  return rp
end

def dedupe_custom_policies_for_dso(resource_type, resource_id, restrict_item)
  rp_array = ResourcePolicy.find_all_by_resource_type_id_and_resource_id_and_rptype(resource_type, resource_id, 'TYPE_CUSTOM')
  #puts "resource type: "+resource_type.to_s+"  id: "+resource_id.to_s
  while rp_array.length > 0
    popped_rp=rp_array.pop()
    remove_array = rp_array.select() do |i_rp|
      i_rp.rpname == popped_rp.rpname && i_rp.epersongroup_id==popped_rp.epersongroup_id && i_rp.rpdescription==popped_rp.rpdescription && i_rp.start_date == popped_rp.start_date
    end
    rp_array = rp_array.reject() do |i_rp|
      i_rp.rpname == popped_rp.rpname && i_rp.epersongroup_id==popped_rp.epersongroup_id && i_rp.rpdescription==popped_rp.rpdescription && i_rp.start_date == popped_rp.start_date
    end
    if !remove_array.nil? and remove_array.length > 0
      remove_array.each() do |doomed_rp|
        #puts "about to destroy "+doomed_rp.policy_id.to_s+" for item "+restrict_item.item_id.to_s
        doomed_rp.destroy
      end
    end
  end
end











