#The purpose here is to collect some code that uses Excel2CSV to read a
#spreadsheet and to make it easy to normalize, extract headers, deal with
#the rows, etc.

require 'excel2csv'

class ExcelReader
  attr_accessor :file, :all_rows, :content_rows, :header_row

  #read the file, remove empty rows, replace nils in non-empty rows with
  #blank strings, strip all strings, and force utf-8 encoding
  def initialize(file)
    self.file = file
    self.read_and_normalize
  end

  #Take the first row for which the block yields true and make it
  #the header row. Make all other rows content rows. Raise an error
  #if none do
  def select_header_row
    header_index = nil
    all_rows.each_with_index do |row, i|
      if yield row
        header_index = i
        break
      end
    end
    raise RuntimeError("No header row found") unless header_index
    self.content_rows = self.all_rows.clone
    self.header_row = self.content_rows.delete_at(header_index)
  end

  #remove any rows for which the block returns true
  def reject_content_rows
    self.content_rows = self.content_rows.reject do |row|
      yield row
    end
  end

  #Replace the header row with the result of calling the block on each entry
  def normalize_headers
    self.header_row = self.header_row.collect do |item|
      yield item
    end
  end

  #return an array of hashes of header => value, one for each content row
  #note that for repeated headers only the last value will survive!
  #we also take no steps to remove unlabelled columns - we assume you'll
  #just ignore them
  def content_hashes
    self.content_rows.collect do |row|
      Hash.new.tap do |hash|
        self.header_row.each_with_index do |header, i|
          hash[header] = row[i]
        end
      end
    end
  end
  
  protected
  
  def read_and_normalize
    sheet = Excel2CSV.read(file)
    rows = sheet.select {|row| row.detect {|item| !blank?(item)}}
    self.all_rows = rows.collect do |row|
      row.collect do |item|
        item ? item.force_encoding('utf-8').strip : ''
      end
    end
  end

  def blank?(object)
    !object || object.strip == ''    
  end
end
