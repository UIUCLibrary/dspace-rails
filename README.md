# dspace-rails

The immediate goal of this project is to expose the Dspace database to ruby (jruby 1.7.4) via Active Record. We believe
that this will facilitate some scripting needs that we have. More specifically it is to expose our own
instantiation (IDEALS). Given that this has some customization there may be models here for tables
that aren't in vanilla DSpace, but we can sort that out later. I don't believe that that alone will
render this unusable for vanilla DSpace.

We use JRuby to have the possibility to call the DSpace Java code as needed.

We use Rails in its entirety in anticipation of the possibility of building some custom administrative things that
we need.

There is a hack to get around the fact that DSpace doesn't use autoincrementing fields for
primary keys. This works for us in Postgres, but might need more consideration for this also
to work with Oracle.

Of course if you use this you're mucking about with the DSpace database without running through
DSpace, so if you change anything there's no way to guarantee that operations that DSpace performs
for consistency, operation with non-database systems, etc. will occur. So be warned. We may work on
that in the future if we need it.