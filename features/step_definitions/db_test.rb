require 'active_record'
require 'test/unit'
## This is a set of steps to ensure that the tables and sequences in the database have owner "dspace"

ActiveRecord::Base.establish_connection(
    :adapter => 'postgresql',
    :database => 'dspace-rails_test',
    :username => 'dspace',
    :password => 'dspace'
)
sql = "SELECT n.nspname as \"Schema\",
  c.relname as \"Name\",
  CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'i' THEN 'index' WHEN 'S' THEN 'sequence' WHEN 's' THEN 'special' WHEN 'f' THEN 'foreign table' END as \"Type\",
  pg_catalog.pg_get_userbyid(c.relowner) as \"Owner\"
  FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE c.relkind IN ('r','v','S','f','')
      AND n.nspname <> 'pg_catalog'
      AND n.nspname <> 'information_schema'
      AND n.nspname !~ '^pg_toast'
      AND pg_catalog.pg_table_is_visible(c.oid)
  ORDER BY 1,2;
  "
records_array = ActiveRecord::Base.connection.execute(sql)

And /^I have a list of table owners$/ do
  records_array.should_not be_nil
end


And /^every entry should be '(.*)'$/ do |user|
  records_array.each do |tableHash|
    tableHash["Owner"].strip.should==user

  end
end




Given(/^the script has been run$/) do
  puts "you did run the script right?"
end



Then(/^this test should pass$/) do
  RestrictItem.all.each do |i|
    item=Item.find(i.item_id)
    puts item.item_id
    puts item.discoverable
    puts i.eperson_group_id
    #if item has no owning collection we should have ignored it
    i.eperson_group_id.should_not==0
    if item.owning_collection.nil?
      item.in_archive.should be_false
      item.discoverable.should be_true
    else
      #if item is fully embargoed
      if i.eperson_group_id.nil?
        if item.item_id!=35082
          item.withdrawn.should be_true
        end
        rp=ResourcePolicy.find_by_resource_type_id_and_resource_id(RESOURCE_TYPE['ITEM'], i.item_id)
        if i.release_date
          rp.start_date.should==i.release_date.to_date
        end
      #if item is closed asccess or limited to a group

      elsif i.eperson_group_id>0
        if i.release_date
          newbundlecount=0
          newstreamcount=0
          bundles=item.getBundleIds
          bundles.each do |id|
            bundle=Bundle.find_by_bundle_id(id)
            if bundle.name=='LICENSE'
              rp=ResourcePolicy.find_by_resource_type_id_and_resource_id(RESOURCE_TYPE['BUNDLE'], id)
              rp.rpname.should be_nil
              rp.start_date.should be_nil
            end
            if bundle.name!='LICENSE'
              puts id
              rp=ResourcePolicy.find_by_resource_type_id_and_resource_id(RESOURCE_TYPE['BUNDLE'], id)
              if rp.start_date==i.release_date.to_date
                newbundlecount+=1
              end
              bitstreams=bundle.getBitstreamIds
              bitstreams.each do |bid|
                puts bid
                rp_array=ResourcePolicy.find_by_resource_type_id_and_resource_id(RESOURCE_TYPE['BITSTREAM'], bid)
                if rp.start_date==i.release_date.to_date
                  newstreamcount+=1
                end
              end
            end
          end
          newstreamcount.should>0
          newbundlecount.should>0
        end
      end
    end
  end
end

