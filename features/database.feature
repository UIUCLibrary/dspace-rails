Feature: Test Database
    In order to make sure dspace is operable
    I want to make sure tables and sequences have owner 'dspace'.

    Scenario: changing table owners
        Given I have a list of table owners
        Then every entry should be 'dspace'